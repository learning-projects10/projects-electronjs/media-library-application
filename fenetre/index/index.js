// ===== Variable et appelle framework electron =====
const {BrowserWindow, app} = require('electron').remote;
const remote = require('electron').remote;
const {ipcRenderer} = require('electron');
let winAddElement;

// ===== Fonctions de création de la fenetre formulaire =====
document.querySelector('#open-formulaire').addEventListener('click', () => {

    winAddElement = new BrowserWindow({
        width: 600, 
        height: 500,
        webPreferences: {
            nodeIntegration: true
        },
        parent: remote.getCurrentWindow(),
        backgroundColor: '#2f3237'
    });

    winAddElement.loadFile(`fenetre/app-element/add-element.html`);
    winAddElement.webContents.openDevTools()
})

// ===== Fonctions qui affiche les musiques ======
document.querySelector('#voir-musiques').addEventListener('click', () => {

    fetch(url)
    .then(function(response){
        return response.text();
    })
    .then(function(contenu){
        fonctionTraitement(contenu);
    })
    .catch(function(error){
        console.log(`Une erreur est survenue ${error}`)
    })

})

// ===== Fonctions d'ajout d'élément dans index.html =====
ipcRenderer.on(`element-added`, elementAdded)
app.whenReady().then(elementAdded);

function elementAdded(){
    let recoverDatasParse = JSON.parse(localStorage.getItem('elements'));

    const elementContainer = document.querySelector('#liste-film');

    elementContainer.innerHTML = "";

    for(let recoverData of recoverDatasParse){
        let html = "";
        html = 
        `<li>
            <img src="../../img/${recoverData.valueFile}">
            <p>${recoverData.valueTitre}</p>
        </li>`;

        elementContainer.innerHTML += html;
    }
}