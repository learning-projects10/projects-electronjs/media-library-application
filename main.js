// ===== Variable et appelle framework electron =====
const {app, BrowserWindow} = require('electron');
let win;

// ===== Fonction de création de fenetre =====
function createWindow () {

    win = new BrowserWindow({
        width: 1000, 
        height: 750,
        webPreferences: {
            nodeIntegration: true
        },
        backgroundColor: '#2f3237'
    });

    win.loadFile(`fenetre/index/index.html`);
    win.webContents.openDevTools()

}

// ===== Methode d'ouverture d'application =====
app.whenReady().then(createWindow);